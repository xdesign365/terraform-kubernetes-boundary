provider "kubernetes" {
  host  = var.config.host
  token = var.config.token
  cluster_ca_certificate = base64decode(var.config.cluster_ca_certificate)
}