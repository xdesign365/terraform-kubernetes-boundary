variable "config" {}

variable "timeouts" {
  default = {
    create = "2m"
    update = "5m"
    delete = "5m"
  }
}

variable "pg_conn" {
  description = "Postgres connection string"
  type = string
}

variable "replicas" {
  default = 1
}